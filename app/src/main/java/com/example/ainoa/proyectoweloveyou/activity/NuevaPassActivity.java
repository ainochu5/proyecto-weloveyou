package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.*;
import com.example.ainoa.proyectoweloveyou.server.ListaUsuarios;
import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.object.Usuario;
import com.example.ainoa.proyectoweloveyou.object.UsuarioS;
import com.example.ainoa.proyectoweloveyou.database.BaseDatos;

public class NuevaPassActivity extends Activity {

    private TextView nuevaPass;
    private SeekBar nivelSeguridad;
    private int contadorNum;
    private int contMayusculas;
    private String nick;
    private CheckBox mostrarPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_pass);
        Button btGuardarCambios = (Button) findViewById(R.id.btNuevaPass);
        btGuardarCambios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprobarPass();
            }
        });
        Bundle bundle = getIntent().getExtras();
        nick = bundle.getString("nick");
        id = bundle.getInt("id");
        nuevaPass = (TextView) findViewById(R.id.eTextNuevaPass);
        nivelSeguridad = (SeekBar) findViewById(R.id.seekBarSeguridad);
        mostrarPass = (CheckBox) findViewById(R.id.ckMostrarPass);
        mostrarPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mostrarPass.isChecked()==true){
                    Toast mensaje = Toast.makeText(getApplicationContext(),nuevaPass.getText().toString(), Toast.LENGTH_SHORT);
                    mensaje.show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();

            builder.setView(inflater.inflate(R.layout.activity_dialogo_personalizado, null))
                    .setPositiveButton(R.string.ocultar, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        return super.onOptionsItemSelected(item);
    }


    public void comprobarPass(){
                if(nuevaPass.getText().toString().isEmpty()){
                    return;
                }
                convertirAAscii(nuevaPass.getText().toString());
                if(contadorNum > 0){
                    nivelSeguridad.setProgress(50);
                }
                if(contMayusculas >0){
                    nivelSeguridad.setProgress(100);
                }
                ListaUsuarios listaU = new ListaUsuarios();
                listaUsuariosS = listaU.devolverAllUsers();
                for(UsuarioS usuarioS : listaUsuariosS){
                    if(usuarioS.getId() == id){
                        listaU.modificar_usuario(nick, id, nuevaPass.getText().toString(),usuarioS);
                    }
                }
        BaseDatos baseDatos = new BaseDatos(this);
        listaUsuarios = baseDatos.devolverAllUsuarios();
       for (Usuario u: listaUsuarios){
            if(u.getId() == idUserServer){
                baseDatos.modificarUsuario(u,u.getNick(),nuevaPass.getText().toString());
            }
        }

                Toast.makeText(this, R.string.cambio_password_correcto, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                contadorNum = 0;
                contMayusculas = 0;

    }

    public void convertirAAscii(String texto){
        for(int i = 0; i < texto.length(); i++){
            if (texto.codePointAt(i) >= 48 && texto.codePointAt(i) <= 57 ){
                contadorNum++;
            }
            if (texto.codePointAt(i) >= 65 && texto.codePointAt(i) <= 90){
                contMayusculas++;
            }
        }
    }
}
