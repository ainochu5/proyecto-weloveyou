package com.example.ainoa.proyectoweloveyou.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.ainoa.proyectoweloveyou.object.Comentario;
import com.example.ainoa.proyectoweloveyou.R;

import java.util.List;

/**
 * Created by Ainoa on 19/12/2015.
 */
public class adapterComentarios extends ArrayAdapter<Comentario> {

    private Context contexto;
    private int layoutId;
    private List<Comentario> datos;

    public adapterComentarios(Context contexto,int layoutId,List<Comentario>datos) {
        super(contexto,layoutId,datos);
        this.contexto = contexto;
        this.layoutId = layoutId;
        this.datos = datos;
    }


    static class ViewHolder{
        TextView comentario;
        ImageButton fotoComentario;
    }

    @Override
    public View getView(int posicion, View view, ViewGroup padre) {

        ViewHolder item = null;

        if (view == null) {
            LayoutInflater inflater = ((Activity) contexto).getLayoutInflater();
            view = inflater.inflate(layoutId, padre, false);

            item = new ViewHolder();
            item.comentario = (TextView)view.findViewById(R.id.textComentario);
            item.fotoComentario = (ImageButton)view.findViewById(R.id.ibComentario);
            view.setTag(item);
        }
        else {
            item = (ViewHolder) view.getTag();
        }

        Comentario comentario = datos.get(posicion);
        item.comentario.setText(comentario.getTexto());
        item.fotoComentario.setImageDrawable(contexto.getResources().getDrawable(R.drawable.comentario));
        return view;
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Comentario getItem(int posicion) {

        return datos.get(posicion);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }
}
