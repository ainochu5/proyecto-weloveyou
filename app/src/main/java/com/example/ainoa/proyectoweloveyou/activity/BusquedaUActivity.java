package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.*;
import com.example.ainoa.proyectoweloveyou.server.ListaUsuarios;
import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.object.UsuarioS;
import com.example.ainoa.proyectoweloveyou.adapter.adapterUsuario;

import java.util.ArrayList;

public class BusquedaUActivity extends Fragment implements View.OnClickListener {

    private EditText textNombreU;
    private ImageButton botonB;
    private ListView listaR;
    private adapterUsuario adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_busqueda_u, container, false);
        textNombreU = (EditText) view.findViewById(R.id.textNombreU);
        botonB = (ImageButton) view.findViewById(R.id.ibBuscarU);
        listaR = (ListView) view.findViewById(R.id.ltResultados);
        botonB.setOnClickListener(this);
        //MENU CONTEXTUAL DENTRO DE LA LISTA
        registerForContextMenu(listaR);
        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.menu_listado, menu);
    }

    //EVENTO DEL MENU CONTEXTUAL QUE ESCUCHA QUE OPCION SE PULSA DEL MENÚ
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        //EN QUE PARTE DE LA LISTA HAS PULSADO
        UsuarioS usuarioSeleccionado = listaUsuariosS.get(info.position);
        switch(item.getItemId()){
            case R.id.action_ver:
                Intent intent = new Intent(getActivity(),DialogoDatosUser.class);
                intent.putExtra("nombre",usuarioSeleccionado.getNombre());
                intent.putExtra("nick",usuarioSeleccionado.getNick());
                intent.putExtra("email",usuarioSeleccionado.getEmail());
                intent.putExtra("fecha",usuarioSeleccionado.getFecha_nacimiento());
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ibBuscarU:
                String nickU = textNombreU.getText().toString();
                //BUSCAR EN EL SERVIDOR POR PARAMETRO
                ListaUsuarios listaU = new ListaUsuarios();
                listaUsuariosS = new ArrayList<>();
                listaUsuariosS = listaU.devolverUsuarios(nickU);
                adapter = new adapterUsuario(getActivity(),R.layout.opinion_item,listaUsuariosS);
                listaR.setAdapter(adapter);
                break;
            default:
                break;
        }
    }
}
