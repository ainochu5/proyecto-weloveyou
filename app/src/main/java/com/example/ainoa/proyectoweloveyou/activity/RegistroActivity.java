package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.ainoa.proyectoweloveyou.server.ListaUsuarios;
import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.object.Usuario;
import com.example.ainoa.proyectoweloveyou.Util.Constantes;
import com.example.ainoa.proyectoweloveyou.Util.GPSTracker;
import com.example.ainoa.proyectoweloveyou.database.BaseDatos;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.nickUsuario;


public class RegistroActivity extends Activity implements View.OnClickListener, LocationListener {

    private EditText textNick;
    private EditText textNombre;
    private EditText textEmail;
    private EditText textTelefono;
    private EditText textFechaN;
    private EditText textPassword;
    private Button btRegistrar;
    private ImageButton imagen;
    private final int RESULTADO_CARGA_IMAGEN = 1;
    private double coordenadas[] = new double[2];
    private ArrayList<Usuario> listaUsuarios;
    private Bundle bundle;
    private GPSTracker gps;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        textNick = (EditText) findViewById(R.id.eTextNick);
        textNombre = (EditText) findViewById(R.id.eTextNombreU);
        textEmail = (EditText) findViewById(R.id.eTextEmail);
        textTelefono = (EditText) findViewById(R.id.eTextTelefono);
        textFechaN = (EditText) findViewById(R.id.eTextFechaN);
        textPassword = (EditText) findViewById(R.id.eTextPassword);
        btRegistrar = (Button) findViewById(R.id.btRegistrar);
        imagen = (ImageButton) findViewById(R.id.ibFotoU);
        imagen.setOnClickListener(this);
        btRegistrar.setOnClickListener(this);
        Constantes.listaUsuarios = new ArrayList<Usuario>();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //CARGAR EL BUNDLE
        bundle = getIntent().getExtras();
        if(bundle!=null){
            String resultado = bundle.getString("modificar");
            BaseDatos baseDatos = new BaseDatos(this);
            listaUsuarios = baseDatos.devolverDatosUsuario(nickUsuario);
            cargarDatos();
        }
        else{
            limpiarDatos();
        }

    }

    private void cargarDatos() {
        textNick.setText(listaUsuarios.get(0).getNick());
        textNombre.setText(listaUsuarios.get(0).getNombre());
        textEmail.setText(listaUsuarios.get(0).getEmail());
        textTelefono.setText(listaUsuarios.get(0).getTelefono());
        textFechaN.setText(listaUsuarios.get(0).getFecha_nacimiento());
        textPassword.setText(listaUsuarios.get(0).getPassword());
        imagen.setImageBitmap(listaUsuarios.get(0).getFoto());

    }
    private void limpiarDatos(){
        textNick.setText("");
        textNombre.setText("");
        textEmail.setText("");
        textTelefono.setText("");
        textFechaN.setText("");
        textPassword.setText("");
        imagen.setImageBitmap(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == RESULTADO_CARGA_IMAGEN) &&
                (resultCode == RESULT_OK) && (data != null)) {
            Uri imagenSeleccionada = data.getData();
            String[] ruta = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(imagenSeleccionada,
                    ruta, null, null, null);
            cursor.moveToFirst();
            int indice = cursor.getColumnIndex(ruta[0]);
            String picturePath = cursor.getString(indice);
            cursor.close();
            ImageButton ibFoto = (ImageButton)
                    findViewById(R.id.ibFotoU);
            ibFoto.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btRegistrar:
                Usuario u = new Usuario();
                u.setNick(textNick.getText().toString());
                u.setNombre(textNombre.getText().toString());
                u.setEmail(textEmail.getText().toString());
                u.setTelefono(textTelefono.getText().toString());
                u.setFecha_nacimiento(textFechaN.getText().toString());
                u.setPassword(textPassword.getText().toString());
                //GUARDAR DATOS GPS
                datosGPS();
                u.setLatitud(coordenadas[0]);
                u.setLongitud(coordenadas[1]);
                u.setFoto(((BitmapDrawable) imagen.getDrawable()).getBitmap());
                ListaUsuarios listaUsuarios = new ListaUsuarios();
                BaseDatos base = new BaseDatos(this);
                if(bundle == null) {
                    long id = base.nuevoUsuario(u);
                    listaUsuarios.anadir_usuario(u,id);
                    Toast mensaje = Toast.makeText(getApplicationContext(), R.string.usuario_creado, Toast.LENGTH_SHORT);
                    mensaje.show();
                    try {
                        base.BD_backup();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Intent intenta = new Intent(this, MainActivity.class);
                    startActivity(intenta);
                }
                else {
                    base.modificarUsuario(u, this.listaUsuarios.get(0).getNick());
                    //listaUsuarios.modificar_usuario(u);
                    Toast mensaje = Toast.makeText(getApplicationContext(), R.string.usuario_modificado, Toast.LENGTH_SHORT);
                    mensaje.show();
                    try {
                        base.BD_backup();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Intent intenta = new Intent(this, PerfilActivity.class);
                    startActivity(intenta);
                }
                break;
            case R.id.ibFotoU:
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, RESULTADO_CARGA_IMAGEN);
                break;
            default:
                break;
        }
    }

    private void datosGPS() {
        gps = new GPSTracker(RegistroActivity.this);

        if(gps.canGetLocation()) {
            coordenadas[0] = gps.getLatitude();
            coordenadas[1] = gps.getLongitude();

            Toast.makeText(
                    getApplicationContext(),
                    "Your Location is -\nLat: " + coordenadas[0] + "\nLong: "
                            + coordenadas[1], Toast.LENGTH_LONG).show();
        } else {
            gps.showSettingsAlert();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
