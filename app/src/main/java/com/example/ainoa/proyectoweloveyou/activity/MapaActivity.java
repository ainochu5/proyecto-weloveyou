package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.listaUsuariosS;

import com.example.ainoa.proyectoweloveyou.server.ListaUsuarios;
import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.object.UsuarioS;
import com.example.ainoa.proyectoweloveyou.Util.Constantes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapaActivity extends Fragment implements GoogleMap.OnMarkerClickListener {

    private GoogleMap mapa;
    private Marker marker;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_mapa, container, false);
        // Inicializa el sistema de mapas de Google
        MapsInitializer.initialize(getActivity());

        // Obtiene una referencia al objeto que permite "manejar" el mapa
        mapa = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        mapa.setOnMarkerClickListener(this);
        marcarUbicaciones();
        return view;
    }
    /**
     * Añade las marcas de todos los usuarios
     */
    private void marcarUbicaciones() {
        ListaUsuarios listaU = new ListaUsuarios();
        listaUsuariosS = listaU.devolverAllUsers();
        System.out.println(listaUsuariosS + "DATOS");
        if (listaUsuariosS.size() > 0) {
            for (UsuarioS usuario: listaUsuariosS) {
                marcarUbicacion(usuario);
            }
        }

        // Posiciona la vista del usuario en Zaragoza
        CameraUpdate camara =
                CameraUpdateFactory.newLatLng(Constantes.SPAIN);

        // Coloca la vista del mapa sobre la posición de la ciudad
        // y activa el zoom para verlo de cerca
        mapa.moveCamera(camara);
        mapa.animateCamera(CameraUpdateFactory.zoomTo(9.0f), 2000, null);
    }
    private void marcarUbicacion(UsuarioS usuario) {

        // Prepara y añade una nueva marca al mapa
        mapa.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marcador))
                .position(new LatLng(usuario.getLatitud(), usuario.getLongitud()))
                .title(usuario.getNombre()));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        this.marker = marker;
        return false;
    }
}
