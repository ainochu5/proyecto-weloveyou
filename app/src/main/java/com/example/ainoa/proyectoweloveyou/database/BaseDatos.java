package com.example.ainoa.proyectoweloveyou.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ainoa.proyectoweloveyou.object.Foto;
import com.example.ainoa.proyectoweloveyou.object.Usuario;
import com.example.ainoa.proyectoweloveyou.Util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.*;

/**
 * Created by Ainoa on 20/11/2015.
 */
public class BaseDatos extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "registrousuarios.db";
    private static final int DATABASE_VERSION = 1;
    //Sentencia SQL para crear la tabla de Usuarios
    // Claúsula SELECT y ORDER BY para utilizar a la hora de consultar los datos
    private static String[] SELECT_CURSOR = {ID_USUARIO, NOMBRE_USUARIO, NICK_USUARIO };
    private static String ORDER_BY = NOMBRE_USUARIO + " DESC";


    public BaseDatos(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //Se ejecuta la sentencia SQL de creación de la tabla
        db.execSQL("CREATE TABLE " + TABLA_USUARIOS + " (" + ID_USUARIO + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                NICK_USUARIO + " TEXT," + NOMBRE_USUARIO + " TEXT," + EMAIL + " TEXT," + TELEFONO + " TEXT," +
                FECHA_NACIMIENTO + " TEXT," + PASSWORD + " TEXT," + FOTO_USUARIO + " BLOB)");

        db.execSQL("CREATE TABLE " + TABLA_FOTOS + " (" + ID_FOTO + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                FOTO_ALBUM + " BLOB," + TITULO_FOTO + " TEXT," + ID_FOTO_PERFIL + " INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*
        En este caso cuando se actualice, borramos la tabla y la volvemos a crear
         */
        //Se elimina la versión anterior de la tabla
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_USUARIOS);
        onCreate(db);

    }

    public long nuevoUsuario(Usuario usuario) {
        //Abrimos la base de datos 'DBUsuarios' en modo escritura
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NICK_USUARIO, usuario.getNick());
        values.put(NOMBRE_USUARIO, usuario.getNombre());
        values.put(EMAIL, usuario.getEmail());
        values.put(TELEFONO, usuario.getTelefono());
        values.put(FECHA_NACIMIENTO, usuario.getFecha_nacimiento().toString());
        values.put(PASSWORD, usuario.getPassword());
        values.put(FOTO_USUARIO, Util.getBytes(usuario.getFoto()));
        long id = db.insert(TABLA_USUARIOS, null, values);
        db.close();
        return id;
    }

    public void nuevaFoto(Foto foto){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(FOTO_ALBUM, Util.getBytes(foto.getFoto_album()));
        values.put(TITULO_FOTO, foto.getTitulo_foto());
        values.put(ID_FOTO_PERFIL, idUserServer);
        db.insert(TABLA_FOTOS, null, values);
        db.close();
    }

    public ArrayList<Foto> devolverFotosUsuario(int id){
        ArrayList<Foto>listaFotos = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Foto foto= null;
        String[] campos = new String[] {ID_FOTO,FOTO_ALBUM,TITULO_FOTO};
        String[] args = new String[] {Integer.toString(id)};
        Cursor c = db.query(TABLA_FOTOS, campos, ID_FOTO_PERFIL+"=?", args, null, null, null);
            //Recorremos el cursor hasta que no haya más registros
            while(c.moveToNext()){
                foto = new Foto();
                System.out.println("existen datos");
                foto.setId(c.getInt(0));
                foto.setFoto_album(Util.getBitmap(c.getBlob(1)));
                foto.setTitulo_foto(c.getString(2));
                listaFotos.add(foto);
            }
        return listaFotos;
    }
    public ArrayList<Foto> devolverAllFotos(){
        ArrayList<Foto>listaFotos = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Foto foto= null;
        String[] campos = new String[] {ID_FOTO,FOTO_ALBUM,TITULO_FOTO,ID_FOTO_PERFIL};
        Cursor c = db.query(TABLA_FOTOS,campos,null, null, null, null, null);
        //Recorremos el cursor hasta que no haya más registros
        while(c.moveToNext()){
            foto = new Foto();
            System.out.println("existen datos");
            foto.setId(c.getInt(0));
            foto.setFoto_album(Util.getBitmap(c.getBlob(1)));
            foto.setTitulo_foto(c.getString(2));
            foto.setId_perfil_u(c.getInt(3));
            listaFotos.add(foto);
        }
        return listaFotos;
    }
    public void modificarUsuario(Usuario usuario, String nick) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NOMBRE_USUARIO, usuario.getNombre());
        values.put(NICK_USUARIO, usuario.getNick());
        values.put(EMAIL, usuario.getEmail());
        values.put(TELEFONO, usuario.getTelefono());
        values.put(FECHA_NACIMIENTO, usuario.getFecha_nacimiento().toString());
        values.put(PASSWORD, usuario.getPassword());
        values.put(FOTO_USUARIO, Util.getBytes(usuario.getFoto()));
        String[] argumentos = new String[]{nick};
        db.update(TABLA_USUARIOS, values, NICK_USUARIO + "=?", argumentos);
        db.close();
    }
    public void modificarUsuario(Usuario usuario, String nick, String password) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NOMBRE_USUARIO, usuario.getNombre());
        values.put(NICK_USUARIO, usuario.getNick());
        values.put(EMAIL, usuario.getEmail());
        values.put(TELEFONO, usuario.getTelefono());
        values.put(FECHA_NACIMIENTO, usuario.getFecha_nacimiento().toString());
        values.put(PASSWORD, password);
        values.put(FOTO_USUARIO, Util.getBytes(usuario.getFoto()));
        String[] argumentos = new String[]{nick};
        db.update(TABLA_USUARIOS, values, NICK_USUARIO + "=?", argumentos);
        db.close();
    }

    public void eliminarUsuario(String nick) {
        SQLiteDatabase db = getWritableDatabase();
        String[] argumentos = new String[]{nick};
        db.delete(TABLA_USUARIOS, NICK_USUARIO + "=?", argumentos);
        db.close();
    }

    public ArrayList<Usuario> devolverAllUsuarios(){
        ArrayList<Usuario>listaDatos = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Usuario u = null;
        String[] campos = new String[] {NICK_USUARIO,NOMBRE_USUARIO,EMAIL,TELEFONO,FECHA_NACIMIENTO,PASSWORD,FOTO_USUARIO};
        Cursor c = db.query(TABLA_USUARIOS, campos, null,null, null, null, null);
        //Recorremos el cursor hasta que no haya más registros
        while(c.moveToNext()){
            u = new Usuario();
            System.out.println("existen datos");
            u.setNick(c.getString(0));
            u.setNombre(c.getString(1));
            u.setEmail(c.getString(2));
            u.setTelefono(c.getString(3));
            u.setFecha_nacimiento(c.getString(4));
            u.setPassword(c.getString(5));
            u.setFoto(Util.getBitmap(c.getBlob(6)));

            listaDatos.add(u);
        }
        return listaDatos;
    }

    public ArrayList<Usuario> devolverDatosUsuario(String nick){
        ArrayList<Usuario>listaDatos = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Usuario u = null;
        String[] campos = new String[] {NICK_USUARIO,NOMBRE_USUARIO,EMAIL,TELEFONO,FECHA_NACIMIENTO,PASSWORD,FOTO_USUARIO};
        String[] args = new String[] {nick};
        Cursor c = db.query(TABLA_USUARIOS, campos, "nick=?", args, null, null, null);
            //Recorremos el cursor hasta que no haya más registros
            while(c.moveToNext()){
                u = new Usuario();
                System.out.println("existen datos");
                u.setNick(c.getString(0));
                u.setNombre(c.getString(1));
                u.setEmail(c.getString(2));
                u.setTelefono(c.getString(3));
                u.setFecha_nacimiento(c.getString(4));
                u.setPassword(c.getString(5));
                u.setFoto(Util.getBitmap(c.getBlob(6)));
                
                listaDatos.add(u);
            }
        return listaDatos;
    }

    public void BD_backup() throws IOException {

        final String inFileName = "/data/data/com.example.ainoa.proyectoweloveyou/databases/"+DATABASE_NAME;
        File dbFile = new File(inFileName);
        FileInputStream fis = null;

        fis = new FileInputStream(dbFile);

        String directorio = "/sdcard/Download";
        File d = new File(directorio);
        if (!d.exists()) {
            d.mkdir();
        }
        String outFileName = directorio + "/"+DATABASE_NAME;

        OutputStream output = new FileOutputStream(outFileName);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer)) > 0) {
            output.write(buffer, 0, length);
        }

        output.flush();
        output.close();
        fis.close();

    }




}
