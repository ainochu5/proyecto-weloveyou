package com.example.ainoa.proyectoweloveyou.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ainoa.proyectoweloveyou.object.Foto;
import com.example.ainoa.proyectoweloveyou.server.ListaUsuarios;
import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.activity.ComentariosActivity;

import java.util.List;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.idFoto;

/**
 * Created by Ainoa on 18/12/2015.
 */
public class adapterFotosTimeLine  extends ArrayAdapter<Foto> {

    private Context contexto;
    private int layoutId;
    private List<Foto> datos;

    public adapterFotosTimeLine(Context contexto,int layoutId,List<Foto>datos) {
        super(contexto,layoutId,datos);
        this.contexto = contexto;
        this.layoutId = layoutId;
        this.datos = datos;
    }
    static class ViewHolder{
        ImageView foto_U;
        EditText textoComentario;
        Button btSubidaComentario;
        Button btVerComentarios;
    }

    @Override
    public View getView(final int posicion, View view, ViewGroup padre) {

        ViewHolder item = null;

        if (view == null) {
            LayoutInflater inflater = ((Activity) contexto).getLayoutInflater();
            view = inflater.inflate(layoutId, padre, false);
            item = new ViewHolder();
            item.foto_U = (ImageView)view.findViewById(R.id.ibFotoTime);
            item.foto_U.setFocusable(false);
            item.btVerComentarios = (Button)view.findViewById(R.id.btComentarios);
            item.textoComentario = (EditText)view.findViewById(R.id.etextComentario);
            item.btSubidaComentario = (Button)view.findViewById(R.id.btSubida);
            final ViewHolder finalItem = item;
            item.btSubidaComentario.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //CARGAR EL ID DEL TIO Y DE SU FOTO
                    String texto = finalItem.textoComentario.getText().toString();
                    ListaUsuarios listaUsuarios = new ListaUsuarios();
                    listaUsuarios.subir_comentario(idFoto,texto);
                    Toast mensaje = Toast.makeText(v.getContext(), R.string.comentario_subido, Toast.LENGTH_SHORT);
                    mensaje.show();
                }
            });
            item.btVerComentarios.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), ComentariosActivity.class);
                    intent.putExtra("id_perfil",datos.get(posicion).getId());
                    getContext().startActivity(intent);
                }
            });

            view.setTag(item);
        }
        else {
            item = (ViewHolder) view.getTag();
        }

        Foto foto = datos.get(posicion);
        item.foto_U.setImageBitmap(foto.getFoto_album());
        return view;
    }

}
