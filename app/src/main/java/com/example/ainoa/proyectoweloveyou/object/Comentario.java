package com.example.ainoa.proyectoweloveyou.object;

/**
 * Created by Ainoa on 19/12/2015.
 */
public class Comentario {

    private int id;
    private String texto;
    private String fotoU;

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFotoU() {
        return fotoU;
    }

    public void setFotoU(String fotoU) {
        this.fotoU = fotoU;
    }
}
