package com.example.ainoa.proyectoweloveyou.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.ainoa.proyectoweloveyou.R;

public class TabActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        loadTabs();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_timeline:
                Intent timeLine = new Intent(this, TimelineActivity.class);
                startActivity(timeLine);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadTabs() {

        Resources res = getResources();

        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Crea las tabs
        ActionBar.Tab tab1 = actionBar.newTab().setText(res.getString(R.string.title_activity_busqueda_u));
        ActionBar.Tab tab2 = actionBar.newTab().setText(res.getString(R.string.title_activity_mapa));

        // Crea cada Fragment para luego asociarla con la pestaña que corresponda
        Fragment tabFragment1 = new BusquedaUActivity();
        Fragment tabFragment2 = new MapaActivity();

        // Asocia cada Fragment con su tab
        tab1.setTabListener(new TabsActivity(tabFragment1));
        tab2.setTabListener(new TabsActivity(tabFragment2));

        // Añade las tabs a la ActionBar
        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
    }
}
