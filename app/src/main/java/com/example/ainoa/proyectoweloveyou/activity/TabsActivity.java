package com.example.ainoa.proyectoweloveyou.activity;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;

import com.example.ainoa.proyectoweloveyou.R;

/**
 * Created by Ainoa on 16/12/2015.
 */
public class TabsActivity implements ActionBar.TabListener{

    private Fragment fragment;

    public TabsActivity(Fragment fragment)
    {
        this.fragment = fragment;
    }

    /**
     * La pestaña se ha re-seleccionado
     */
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

        // Aquí por ejemplo se podría refrescar la vista
    }

    /**
     * Se ha seleccionado una pestaña. Se carga en la pantalla
     */
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

        ft.replace(R.id.container, fragment);
    }

    /**
     * Se ha deseleccionado una pestaña. Se elimina de la pantalla
     */
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

        ft.remove(fragment);
    }
}
