package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.*;

import com.example.ainoa.proyectoweloveyou.server.ListaUsuarios;
import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.object.UsuarioS;

public class RecuperarContrasenaActivity extends Activity implements View.OnClickListener {

    private TextView direccionRecuperacion;
    private TextView movilRecuperacion;
    private String movil;
    private String direccion;
    private int notificationID = 1;
    private boolean correcto;
    private String nickU;
    private int idU;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_contrasena);
        Button realizarPeticion = (Button) findViewById(R.id.btEnviarPeticion);
        direccionRecuperacion = (TextView) findViewById(R.id.eTextCorreoRecuperacion);
        movilRecuperacion = (TextView) findViewById(R.id.eTextMovilRecuperacion);
        realizarPeticion.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();

            builder.setView(inflater.inflate(R.layout.activity_dialogo_personalizado, null))
                    .setPositiveButton(R.string.ocultar, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        movil = movilRecuperacion.getText().toString();
        direccion = direccionRecuperacion.getText().toString();
        if(!movil.isEmpty() || direccion.isEmpty()) {
            //COMPROBAR SI LOS DATOS SON CORRECTOS
            ListaUsuarios listaU = new ListaUsuarios();
            listaUsuariosS = listaU.devolverAllUsers();
            //RECORRO EL FOR
            for (UsuarioS usuario : listaUsuariosS){
                if(usuario.getTelefono().equals(movil) && usuario.getEmail().equals(direccion)){
                    correcto = true;
                    nickU = usuario.getNick();
                    idU = usuario.getId();
                    idUserServer = usuario.getId_foto_u();
                }
            }
            if (correcto == true){
                mostrarNotificacion();
                Intent intent = new Intent(this, NuevaPassActivity.class);
                intent.putExtra("nick",nickU);
                intent.putExtra("id",idU);
                startActivity(intent);
                correcto = false;
            }
            else{
                Toast.makeText(this, R.string.datos_incorrectos, Toast.LENGTH_SHORT).show();
                movilRecuperacion.setText("");
                direccionRecuperacion.setText("");
            }

        }
        else{
            Toast.makeText(this, R.string.telefono_email_incorrectos, Toast.LENGTH_SHORT).show();
            movilRecuperacion.setText("");
            direccionRecuperacion.setText("");
        }
    }

    private void mostrarNotificacion() {
        Intent i = new Intent(this, RecuperarContrasenaActivity.class);
        i.putExtra("notificationID", notificationID);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, 0);
        NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        CharSequence ticker =getString(R.string.cambio_password_para) + direccion;
        CharSequence contentTitle = getString(R.string.solicitud_nueva_pass);
        CharSequence contentText = getString(R.string.usuario_numero) + movil + getString(R.string.cambio_password);
        Notification noti = new NotificationCompat.Builder(this)
                .setContentIntent(pendingIntent)
                .setTicker(ticker)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setSmallIcon(R.drawable.logo)
                .setVibrate(new long[] {100, 250, 100, 500})
                .build();
        nm.notify(notificationID, noti);
    }
}
