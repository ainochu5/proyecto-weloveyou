package com.example.ainoa.proyectoweloveyou.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.object.UsuarioS;

import java.util.List;

/**
 * Created by Ainoa on 14/12/2015.
 */
public class adapterUsuario extends ArrayAdapter<UsuarioS> {

    private Context contexto;
    private int layoutId;
    private List<UsuarioS>datos;

    public adapterUsuario(Context contexto,int layoutId,List<UsuarioS>datos) {
        super(contexto,layoutId,datos);
        this.contexto = contexto;
        this.layoutId = layoutId;
        this.datos = datos;
    }


    static class ViewHolder{
        TextView nombre;
    }

    @Override
    public View getView(int posicion, View view, ViewGroup padre) {

        ViewHolder item = null;

        if (view == null) {
            LayoutInflater inflater = ((Activity) contexto).getLayoutInflater();
            view = inflater.inflate(layoutId, padre, false);

            item = new ViewHolder();
            item.nombre = (TextView)view.findViewById(R.id.tvNombreUsuario);

            view.setTag(item);
        }
        else {
            item = (ViewHolder) view.getTag();
        }

        UsuarioS usuario = datos.get(posicion);
        item.nombre.setText(usuario.getNombre());

        return view;
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public UsuarioS getItem(int posicion) {

        return datos.get(posicion);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }

}
