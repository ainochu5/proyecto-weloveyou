package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import static com.example.ainoa.proyectoweloveyou.Util.Constantes.*;
import com.example.ainoa.proyectoweloveyou.R;

public class DialogoDatosUser extends Activity {

    private ImageView fotoU;
    private TextView nombreU;
    private TextView nickU;
    private TextView emailU;
    private TextView fechaU;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialogo_datos_user);
        Bundle bundle = getIntent().getExtras();
        nombreU = (TextView) findViewById(R.id.textNombreU);
        nickU = (TextView) findViewById(R.id.textNickU);
        emailU = (TextView) findViewById(R.id.textCorreU);
        fechaU = (TextView) findViewById(R.id.textFechaN);
        nombreU.setText(bundle.getString("nombre"));
        nickU.setText(bundle.getString("nick"));
        emailU.setText(bundle.getString("email"));
        fechaU.setText(bundle.getString("fecha"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();

            builder.setView(inflater.inflate(R.layout.activity_dialogo_personalizado, null))
                    .setPositiveButton(R.string.ocultar, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

}
