package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.ainoa.proyectoweloveyou.server.ListaUsuarios;
import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.adapter.adapterComentarios;

import java.util.ArrayList;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.listaComentarios;

public class ComentariosActivity extends Activity {

    private ListView ltComentarios;
    private adapterComentarios adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialogo_comentarios);
        //COMPRUEBO EL BUNDLE
        Bundle bundle = getIntent().getExtras();
        int id_foto = bundle.getInt("id_perfil");
        ListaUsuarios listaU = new ListaUsuarios();
        listaComentarios = new ArrayList<>();
        listaComentarios = listaU.devolver_comentarios_por_foto(id_foto);
        ltComentarios = (ListView) findViewById(R.id.ltComentarios);
        adapter = new adapterComentarios(this,R.layout.comentario,listaComentarios);
        ltComentarios.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_comentarios, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()){
            case R.id.action_timeline:
                Intent timeLine = new Intent(this,TimelineActivity.class);
                startActivity(timeLine);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
