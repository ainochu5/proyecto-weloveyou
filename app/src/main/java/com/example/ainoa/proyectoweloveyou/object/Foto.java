package com.example.ainoa.proyectoweloveyou.object;

import android.graphics.Bitmap;

/**
 * Created by Ainoa on 14/12/2015.
 */
public class Foto {
    private int id;
    private Bitmap foto_album;
    private String titulo_foto;
    private int id_perfil_u;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bitmap getFoto_album() {
        return foto_album;
    }

    public void setFoto_album(Bitmap foto_album) {
        this.foto_album = foto_album;
    }

    public String getTitulo_foto() {
        return titulo_foto;
    }

    public void setTitulo_foto(String titulo_foto) {
        this.titulo_foto = titulo_foto;
    }

    public int getId_perfil_u() {
        return id_perfil_u;
    }

    public void setId_perfil_u(int id_perfil_u) {
        this.id_perfil_u = id_perfil_u;
    }
}
