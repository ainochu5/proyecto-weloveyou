package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.*;

import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.adapter.adapterFotosPerfil;
import com.example.ainoa.proyectoweloveyou.database.BaseDatos;

public class PerfilActivity extends Activity implements View.OnClickListener {

    Button btModificarPerfil;
    private adapterFotosPerfil adapter;
    private ImageView ivfotoPerfil;
    private TextView textNombreU;
    private TextView textNPublicaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        //PILLAR LOS DATOS DEL INTENT
        btModificarPerfil = (Button) findViewById(R.id.btModificarPerfil);
        btModificarPerfil.setOnClickListener(this);
        ListView lvFotosU = (ListView) findViewById(R.id.ltPublicaciones);
        BaseDatos baseDatos = new BaseDatos(this);
        ivfotoPerfil = (ImageView) findViewById(R.id.ivFotoPerfil);
        //CAMBIAR LA FOTO A FORMA CIRCULAR

        textNombreU = (TextView) findViewById(R.id.textNombreU);
        textNPublicaciones = (TextView) findViewById(R.id.tvNumPublicaciones);
        listaUsuarios = baseDatos.devolverDatosUsuario(nickUsuario);
        listaUsuarios = baseDatos.devolverDatosUsuario(nickUsuario);
        listaFotos = baseDatos.devolverFotosUsuario(idUserServer);
        ivfotoPerfil.setImageBitmap(listaUsuarios.get(0).getFoto());
        textNombreU.setText(listaUsuarios.get(0).getNombre());
        textNPublicaciones.setText(""+listaFotos.size());
        adapter = new adapterFotosPerfil(this,R.layout.foto_perfil,listaFotos);
        lvFotosU.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_perfil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_busqueda:
                Intent busqueda = new Intent(this,TabActivity.class);
                startActivity(busqueda);
                break;
            case R.id.action_timeline:
                Intent timeLine = new Intent(this,TimelineActivity.class);
                startActivity(timeLine);
                break;
            case R.id.action_borrar:
                final BaseDatos baseDatos = new BaseDatos(this);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.lb_eliminar_cuenta)
                        .setPositiveButton(R.string.lb_eliminar_si, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                baseDatos.eliminarUsuario("ainochu");
                                dialog.dismiss();
                                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                                startActivity(intent);
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btModificarPerfil:
                Intent intent = new Intent(this,RegistroActivity.class);
                intent.putExtra("modificar","true");
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
