package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.adapter.adapterFotosTimeLine;
import com.example.ainoa.proyectoweloveyou.database.BaseDatos;

import java.util.ArrayList;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.idFoto;
import static com.example.ainoa.proyectoweloveyou.Util.Constantes.listaFotosTimeline;

/**
 * Created by Ainoa on 01/12/2015.
 */
public class TimelineActivity extends Activity {
    private ListView publicaciones;
    private adapterFotosTimeLine adapter;
    private Button btSubida;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_line);        publicaciones = (ListView) findViewById(R.id.lvPublicaciones);
        BaseDatos baseDatos = new BaseDatos(this);
        listaFotosTimeline = new ArrayList<>();
        listaFotosTimeline = baseDatos.devolverAllFotos();
        adapter = new adapterFotosTimeLine(this,R.layout.foto_timeline,listaFotosTimeline);
        publicaciones.setAdapter(adapter);
        publicaciones.setClickable(true);
        publicaciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idFoto = position + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_time_line, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_casa:
                Intent perfil = new Intent(this,PerfilActivity.class);
                startActivity(perfil);
                break;
            case R.id.action_busqueda:
                Intent busqueda = new Intent(this,TabActivity.class);
                startActivity(busqueda);
                break;
            case R.id.action_nuevafoto:
                Intent newfoto = new Intent(this,SubidaFotoActivity.class);
                startActivity(newfoto);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }



}
