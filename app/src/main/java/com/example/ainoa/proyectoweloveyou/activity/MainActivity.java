package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.*;

import com.example.ainoa.proyectoweloveyou.server.ListaUsuarios;
import com.example.ainoa.proyectoweloveyou.R;

import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {

    private Button btaceptar;
    private Button btrecuperarPass;
    private Button btRegistrar;
    private Button btMapa;
    private EditText nick;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        btaceptar = (Button) findViewById(R.id.btLogin);
        btrecuperarPass = (Button) findViewById(R.id.btRecuperarPassword);
        btRegistrar = (Button) findViewById(R.id.btRegistrar);
        nick = (EditText) findViewById(R.id.eTextNick);
        password = (EditText) findViewById(R.id.eTextPassword);
        btaceptar.setOnClickListener(this);
        btRegistrar.setOnClickListener(this);
        btrecuperarPass.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();

            builder.setView(inflater.inflate(R.layout.activity_dialogo_personalizado, null))
                    .setPositiveButton(R.string.ocultar, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch(view.getId()){
            case R.id.btLogin:
                String nickU = nick.getText().toString();
                String passU = password.getText().toString();
                ListaUsuarios listaU = new ListaUsuarios();
                listaUsuariosS = new ArrayList<>();
                listaUsuariosS= listaU.devolverUsuarios(nickU);
                if(listaUsuariosS.size()>0){
                    if(!listaUsuariosS.get(0).getPassword().equals(passU)){
                        Toast.makeText(this, R.string.user_pass_incorrecto, Toast.LENGTH_SHORT).show();
                        nick.setText("");
                        password.setText("");
                    }
                    else{
                        nickUsuario = listaUsuariosS.get(0).getNick();
                        idUserServer = listaUsuariosS.get(0).getId();
                        Toast.makeText(this, nickUsuario, Toast.LENGTH_SHORT).show();
                        intent = new Intent(this,TimelineActivity.class);
                        startActivity(intent);
                    }
        }
                break;
            case R.id.btRecuperarPassword:
                intent = new Intent(this,RecuperarContrasenaActivity.class);
                startActivity(intent);
                break;
            case R.id.btRegistrar:
                intent = new Intent(this,RegistroActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

    }
}
