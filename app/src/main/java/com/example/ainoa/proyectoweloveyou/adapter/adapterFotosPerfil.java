package com.example.ainoa.proyectoweloveyou.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;

import com.example.ainoa.proyectoweloveyou.object.Foto;
import com.example.ainoa.proyectoweloveyou.R;

import java.util.List;

/**
 * Created by Ainoa on 18/12/2015.
 */
public class adapterFotosPerfil extends ArrayAdapter<Foto> {

    private Context contexto;
    private int layoutId;
    private List<Foto> datos;

    public adapterFotosPerfil(Context contexto,int layoutId,List<Foto>datos) {
        super(contexto,layoutId,datos);
        this.contexto = contexto;
        this.layoutId = layoutId;
        this.datos = datos;
    }
    static class ViewHolder{
        ImageButton foto_U;
    }

    @Override
    public View getView(int posicion, View view, ViewGroup padre) {

        ViewHolder item = null;

        if (view == null) {
            LayoutInflater inflater = ((Activity) contexto).getLayoutInflater();
            view = inflater.inflate(layoutId, padre, false);

            item = new ViewHolder();
            item.foto_U = (ImageButton)view.findViewById(R.id.ibFotoPerfil);

            view.setTag(item);
        }
        else {
            item = (ViewHolder) view.getTag();
        }

        Foto foto = datos.get(posicion);
        item.foto_U.setImageBitmap(foto.getFoto_album());
        return view;
    }

}
