package com.example.ainoa.proyectoweloveyou.server;
import android.content.ContentValues;

import com.example.ainoa.proyectoweloveyou.object.Comentario;
import com.example.ainoa.proyectoweloveyou.object.Usuario;
import com.example.ainoa.proyectoweloveyou.object.UsuarioS;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.example.ainoa.proyectoweloveyou.Util.Constantes.*;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Ainoa on 12/12/2015.
 */

public class ListaUsuarios {

    private String rutaFoto;
    private String foto;
    private byte[] byteArray;

    /**
     * Devuelve la lista de usuarios con sus datos a un arrayList de la clase usuario
     */

    public ArrayList<UsuarioS> devolverAllUsers(){
        // Llamada a un servicio web y recogida de los datos que devuelve
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        UsuarioS[] Usuarios = restTemplate.getForObject(SERVER_URL + "/usuarios", UsuarioS[].class);
        listaUsuariosS.addAll(Arrays.asList(Usuarios));
        return listaUsuariosS;
    }

    public ArrayList<UsuarioS> devolverUsuarios(String nick){
        // Llamada a un servicio web y recogida de los datos que devuelve
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        UsuarioS[] Usuarios = restTemplate.getForObject(SERVER_URL + "/usuario_login?nick="+nick, UsuarioS[].class);
        listaUsuariosS.addAll(Arrays.asList(Usuarios));
        return listaUsuariosS;
    }
    public void modificar_usuario(String nick, int id, String password, UsuarioS usuarioS){
        // Llamada a un servicio web y recogida de los datos que devuelve
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getForObject(SERVER_URL + "/modificar_usuario?id=" + id + "&nombre=" + usuarioS.getNombre()+"&nick=" + nick + "&email="+usuarioS.getEmail()
                +"&telefono="+usuarioS.getTelefono()+"&password=" + password + "&latitud="+usuarioS.getLatitud()+"&longitud="+usuarioS.getLongitud(), Void.class);
    }

    public void modificar_usuario(Usuario usuario){
        // Llamada a un servicio web y recogida de los datos que devuelve
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getForObject(SERVER_URL + "/modificar_usuario?id=" + usuario.getId() + "&nombre=" + usuario.getNombre() + "&nick=" + usuario.getNick() + "" +
                "&email=" + usuario.getEmail() + "&telefono=" + usuario.getTelefono() + "&password=" + usuario.getPassword() +
                        "&latitud=" + usuario.getLatitud() + "&longitud=" + usuario.getLongitud() + "&id_foto_u=" + idUserServer, Void.class);
    }

    public void anadir_usuario(Usuario usuario, long id_foto){
        id = (int) id_foto;
        // Llamada a un servicio web y recogida de los datos que devuelve
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        ContentValues values = new ContentValues();
        restTemplate.getForObject(SERVER_URL + "/add_usuario?nombre=" + usuario.getNombre() + "&nick=" + usuario.getNick() + "" +
                "&email=" + usuario.getEmail() + "&telefono=" + usuario.getTelefono() + "&password=" + usuario.getPassword() +
                "&latitud=" + usuario.getLatitud() + "&longitud=" + usuario.getLongitud() + "&id_foto_u=" + id, Void.class);
    }

    public void subir_comentario(int id, String mensaje){
        // Llamada a un servicio web y recogida de los datos que devuelve
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        ContentValues values = new ContentValues();
        restTemplate.getForObject(SERVER_URL + "/add_comentario?texto="+mensaje+"&fotoU="+id,Void.class);
    }

    public ArrayList<Comentario> devolver_comentarios_por_foto(int id_foto){
        // Llamada a un servicio web y recogida de los datos que devuelve
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Comentario[] comentarios = restTemplate.getForObject(SERVER_URL + "/comentario_por_id?fotoU="+id_foto, Comentario[].class);
        listaComentarios.addAll(Arrays.asList(comentarios));
        return listaComentarios;
    }



}
