package com.example.ainoa.proyectoweloveyou.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.ainoa.proyectoweloveyou.object.Foto;
import com.example.ainoa.proyectoweloveyou.R;
import com.example.ainoa.proyectoweloveyou.database.BaseDatos;

import java.io.IOException;

public class SubidaFotoActivity extends Activity implements View.OnClickListener {

    private ImageButton fotoSubida;
    private EditText tituloFoto;
    private Button btSubida;
    private final int RESULTADO_CARGA_IMAGEN = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subida_foto);
        fotoSubida = (ImageButton) findViewById(R.id.ibFotoSubida);
        tituloFoto = (EditText) findViewById(R.id.textTituloFoto);
        btSubida = (Button) findViewById(R.id.btSubirFoto);
        fotoSubida.setOnClickListener(this);
        btSubida.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == RESULTADO_CARGA_IMAGEN) &&
                (resultCode == RESULT_OK) && (data != null)) {
            Uri imagenSeleccionada = data.getData();
            String[] ruta = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(imagenSeleccionada,
                    ruta, null, null, null);
            cursor.moveToFirst();
            int indice = cursor.getColumnIndex(ruta[0]);
            String picturePath = cursor.getString(indice);
            cursor.close();
            ImageButton ibFoto = (ImageButton)
                    findViewById(R.id.ibFotoSubida);
            ibFoto.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_subida_foto, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btSubirFoto:
                Foto f = new Foto();
                f.setTitulo_foto(tituloFoto.getText().toString());
                f.setFoto_album(((BitmapDrawable) fotoSubida.getDrawable()).getBitmap());
                BaseDatos baseDatos = new BaseDatos(this);
                baseDatos.nuevaFoto(f);
                try {
                    baseDatos.BD_backup();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast mensaje = Toast.makeText(getApplicationContext(), R.string.foto_subida, Toast.LENGTH_SHORT);
                mensaje.show();
                break;
            case R.id.ibFotoSubida:
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, RESULTADO_CARGA_IMAGEN);
                break;
            default:
                break;
        }
    }
}
