package com.example.ainoa.proyectoweloveyou.object;

/**
 * Created by Ainoa on 19/12/2015.
 */
public class UsuarioS {

    private int id;
    private String nick;
    private String nombre;
    private String email;
    private String telefono;
    private String fecha_nacimiento;
    private String password;
    private double latitud;
    private double longitud;
    private int id_foto_u;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getId_foto_u() {
        return id_foto_u;
    }

    public void setId_foto_u(int id_foto_u) {
        this.id_foto_u = id_foto_u;
    }
}
